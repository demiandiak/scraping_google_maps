import argparse
import csv
import re
import time


class Parser:

    def __init__(self, *search_words):
        from selenium import webdriver
        self.request = " ".join(search_words)
        self.url = 'https://www.google.com.ua/maps/search/' + "+".join(search_words) + '/'
        self.title_xpath = "/html/body/jsl/div[3]/div[9]/div[8]/div/div[1]/div/div/div[4]/div[1]"
        self.class_el = 'a4gq8e-aVTXAb-haAclf-jRmmHf-hSRGPd'
        self.id_pagg_button = 'ppdPk-Ej1Yeb-LgbsSe-tJiF1e'
        self.driver = webdriver.Chrome()
        self.dicts = {}
        self.institutions = []

    def get_urls(self):
        from selenium.webdriver.common.action_chains import ActionChains

        self.driver.get(self.url)
        but_next_page = self.driver.find_element_by_id(self.id_pagg_button)
        while not but_next_page.get_attribute('disabled') or not but_next_page:
            self.institutions_urls()
            from selenium.common.exceptions import ElementNotInteractableException
            try:
                ActionChains(self.driver).click(but_next_page).perform()
            except ElementNotInteractableException as e:
                print(e)
                break
            time.sleep(1)

    def institutions_urls(self):
        from selenium.common.exceptions import NoSuchElementException
        try:
            div_list = self.driver.find_element_by_xpath(self.title_xpath)
            for _ in range(3):
                self.driver.execute_script("arguments[0].scrollTop = arguments[0].scrollHeight", div_list)
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                time.sleep(1)
            elements = self.driver.find_elements_by_class_name(self.class_el)
            for web in elements[-20:]:
                title = web.get_attribute('aria-label')
                if title not in self.dicts:
                    self.dicts[title] = web.get_attribute('href')
        except NoSuchElementException as e:
            print(e)

    def parser_institution(self, item):
        from selenium.common.exceptions import NoSuchElementException
        self.driver.get(item[1])
        time.sleep(2)   # замінити очікувач завантаження
        telephone, site = None, None
        for number in range(9, 0, -1):
            try:
                el = self.driver.find_element_by_xpath(f'//*[@id="pane"]/div/div[1]/div/div/div[9]/div[{number}]/button/div[1]/div[2]/div[1]').text.replace(" ", "")
            except NoSuchElementException:
                continue
            if el.isdigit():
                telephone = "+38" + el
            elif re.match(r"([a-z0-9-]+\.?){2,4}", el):
                site = el
                break

        self.institutions.append((item[0].replace('\u02b9', '').replace('\U0001f363', ''), site, telephone, item[1]))

    def get_table(self):
        for item in self.dicts.items():
            self.parser_institution(item)

    def export(self):
        with open(f'{self.request}.csv', 'wt', newline='') as file:
            writer = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(('title', 'site', 'telephone', 'gmaps url'), )
            writer.writerows(self.institutions)


def main():
    parser = argparse.ArgumentParser(description='words')
    parser.add_argument('words', type=str, help='words')
    arguments = parser.parse_args()
    s = Parser(*arguments.words.split())
    s.get_urls()
    s.get_table()
    s.export()


if __name__ == "__main__":
    main()
